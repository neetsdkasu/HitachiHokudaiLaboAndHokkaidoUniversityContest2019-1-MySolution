@echo off
setlocal
set solver=target\debug\first.exe

set tcdir=res_A
if not exist %tcdir% mkdir %tcdir%

set tcin=%tcdir%\tc%1.in
set tcout=%tcdir%\tc%1.out
set tcerr=%tcdir%\tc%1.err

if exist %tcin% goto judge

generator.exe %tcin% 1 %1
if errorlevel 1 goto errorgenerate

:judge
echo SEED = %1

%solver% 0< %tcin% 1> %tcout% 2> %tcerr%
if errorlevel 1 goto errorfailed
output_checker.exe %tcin% %tcout% 1>> %tcerr%
type %tcerr%

goto fileend

:errorgenerate
echo failed generate testcase "%1"
goto fileend

:errorfailed
echo failed running program
if exist %tcerr% type %tcerr%
goto fileend


:fileend
endlocal

