#!/bin/sh

solver='./target/debug/first.exe'
resdir=res_B

mkdir -p $resdir

echo Seed: $1

python3 ./interactive.py --judge "./judge.exe 1 $1" --contestant "$solver" 1> ./$resdir/tester$1.log 2> ./$resdir/result$1.log

cat ./$resdir/result$1.log

mv ./judge.out ./$resdir/judge$1.out

mv ./contestant.out ./$resdir/contestant$1.out


